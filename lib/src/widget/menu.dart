import 'package:flutter/material.dart';
import 'package:flutter_application_1/src/pages/detallesAmiibo.dart';
import 'package:flutter_application_1/src/pages/favorito.dart';
import 'package:flutter_application_1/src/pages/home.dart';
import 'package:flutter_application_1/src/widget/opcionesMenu.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() => runApp(MenuWidget());

class MenuWidget extends StatefulWidget {
  MenuWidget({Key key}) : super(key: key);
  _MenuWidgetState createState() => _MenuWidgetState();
}

Color colorMenu = Colors.yellow;

class _MenuWidgetState extends State<MenuWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 10,
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                // height: size.height * .2,
                child: DrawerHeader(
                    decoration: BoxDecoration(
                        //color: Color.fromRGBO(255, 255, 255, 1),
                        ),
                    child: Center(
                      child: Text("User"),
                    )),
              ),
              Container(
                // height: size.height * .77,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      OpcionesMenu(
                        icon: FontAwesomeIcons.home,
                        nivel: 1,
                        nombre: "Inicio",
                        onTap: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePage()),
                          );
                        },
                      ),
                      OpcionesMenu(
                        icon: FontAwesomeIcons.heart,
                        nivel: 1,
                        nombre: "Favoritos",
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Favoritos()),
                          );
                        },
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
