import 'package:flutter/material.dart';
import 'package:flutter_application_1/src/pages/detallesAmiibo.dart';
import 'package:flutter_application_1/src/static/globals.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Favoritos extends StatefulWidget {
  @override
  _FavoritosState createState() => _FavoritosState();
}

class _FavoritosState extends State<Favoritos> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favoritos"),
      ),
      body: Container(
        child: SingleChildScrollView(
                  child: Wrap(
            children: favoritos.map((e) => GestureDetector(
                                          onTap: () {
                                            print(e.toJson());
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetallesAmiibo(amiiboModel: e,)),
                                            );
                                          },
                                          child: Container(
                                            height: 180.0,
                                            width: 180.0,
                                            child: Container(
                                                margin: EdgeInsets.all(5),
                                                decoration: BoxDecoration(
                                                  color: e.color,
                                                  borderRadius:
                                                      BorderRadius.circular(20.0),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.black26,
                                                      offset: Offset(0.0, 2.0),
                                                      blurRadius: 6.0,
                                                    ),
                                                  ],
                                                ),
                                                child: Stack(
                                                  children: [
                                                    Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.center,
                                                      children: [
                                                        Container(
                                                          width: 200,
                                                          decoration: BoxDecoration(
                                                            color: Colors.brown,
                                                            borderRadius:
                                                                BorderRadius.circular(
                                                                    20.0),
                                                            boxShadow: [
                                                              BoxShadow(
                                                                color: Colors.black26,
                                                                offset:
                                                                    Offset(0.0, 2.0),
                                                                blurRadius: 6.0,
                                                              ),
                                                            ],
                                                          ),
                                                          child: Column(
                                                            children: [
                                                              Text(
                                                                e.amiiboSeries,
                                                                style: TextStyle(
                                                                    color:
                                                                        Colors.white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                              Text(
                                                                e.name,
                                                                style: TextStyle(
                                                                    color:
                                                                        Colors.white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                                textAlign:
                                                                    TextAlign.center,
                                                              ),
                                                              // Text(
                                                              //   e.tail,
                                                              //   style: TextStyle(
                                                              //       color:
                                                              //           Colors.white,
                                                              //       fontWeight:
                                                              //           FontWeight
                                                              //               .w400),
                                                              //   textAlign:
                                                              //       TextAlign.center,
                                                              // ),
                                                            ],
                                                          ),
                                                        ),
                                                        ClipRRect(
                                                          child: Image(
                                                            height: 121.0,
                                                            width: 121.0,
                                                            image:
                                                                NetworkImage(e.image),
                                                            fit: BoxFit.contain,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.all(1),
                                                      alignment: Alignment.bottomLeft,
                                                      child: IconButton(
                                                        icon: Icon(FontAwesomeIcons.solidHeart,color: e.favorito == true? Colors.red:Colors.white60,),
                                                        onPressed: (){
                                                          setState(() {
                                                            e.favorito =true;
                                                          });
                                                          favoritos.add(e);
                                                        },
                                                      ))
                                                  ],
                                                )),
                                          ),
                                        )).toList(),
          ),
        ),
      ),
    );
  }
}