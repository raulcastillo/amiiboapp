
import 'package:flutter/material.dart';
import 'package:flutter_application_1/src/models/amiibo_model.dart';
import 'package:flutter_application_1/src/pages/detallesAmiibo.dart';
import 'package:flutter_application_1/src/providers/amiibo_provider.dart';
import 'package:flutter_application_1/src/static/globals.dart';
import 'package:flutter_application_1/src/widget/buscador.dart';
import 'package:flutter_application_1/src/widget/menu.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<AmiiboModel> amibo = List<AmiiboModel>.empty();
  int inicio = 0;
  int fin = 8;
  ScrollController controller;
  List<AmiiboModel> listBusqueda = List<AmiiboModel>.empty();
  int estado;
  TextEditingController textBuscar = TextEditingController();
  @override
  void initState() {
    super.initState();
    obtenerAmibos();
    controller = ScrollController();
  }

  obtenerAmibos() async {
    await obtenerAmiibo().then((value) {
      setState(() {
        amibo = value;
      });
      listBusqueda = contadorList(fin);
    });
  }

  loadMore() {
    print("fin");
    setState(() {
      fin = fin + 4;
      listBusqueda = contadorList(fin);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 40,
          child: Image.asset(
            "assets/logo-amiibo-glow.png",
            fit: BoxFit.cover,
          ),
        ),
        centerTitle: true,
      ),
      drawer: MenuWidget(),
      body: listBusqueda.length > 0
          ? Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                      color: Colors.white,
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Search(
                        controller: textBuscar,
                        hintText: "buscar por personaje",
                        onChanged: (String v) {
                          if (v.isNotEmpty) {
                            setState(() {
                              listBusqueda = contadorList(fin);
                            });
                          }
                        },
                      )),
                  Expanded(
                    child: LazyLoadScrollView(
                      onEndOfPage: () => loadMore(),
                      child: ListView(
                        controller: controller,
                        children: [
                          Center(
                            child: Wrap(
                              children: listBusqueda
                                  .map((e) => GestureDetector(
                                        onTap: () {
                                          print(e.toJson());
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetallesAmiibo(amiiboModel: e,)),
                                          );
                                        },
                                        child: Container(
                                          height: 180.0,
                                          width: 180.0,
                                          child: Container(
                                              margin: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                color: e.color,
                                                borderRadius:
                                                    BorderRadius.circular(20.0),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.black26,
                                                    offset: Offset(0.0, 2.0),
                                                    blurRadius: 6.0,
                                                  ),
                                                ],
                                              ),
                                              child: Stack(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.center,
                                                    children: [
                                                      Container(
                                                        width: 200,
                                                        decoration: BoxDecoration(
                                                          color: Colors.brown,
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  20.0),
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: Colors.black26,
                                                              offset:
                                                                  Offset(0.0, 2.0),
                                                              blurRadius: 6.0,
                                                            ),
                                                          ],
                                                        ),
                                                        child: Column(
                                                          children: [
                                                            Text(
                                                              e.amiiboSeries,
                                                              style: TextStyle(
                                                                  color:
                                                                      Colors.white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                            Text(
                                                              e.name,
                                                              style: TextStyle(
                                                                  color:
                                                                      Colors.white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400),
                                                              textAlign:
                                                                  TextAlign.center,
                                                            ),
                                                            // Text(
                                                            //   e.tail,
                                                            //   style: TextStyle(
                                                            //       color:
                                                            //           Colors.white,
                                                            //       fontWeight:
                                                            //           FontWeight
                                                            //               .w400),
                                                            //   textAlign:
                                                            //       TextAlign.center,
                                                            // ),
                                                          ],
                                                        ),
                                                      ),
                                                      ClipRRect(
                                                        child: Image(
                                                          height: 121.0,
                                                          width: 121.0,
                                                          image:
                                                              NetworkImage(e.image),
                                                          fit: BoxFit.contain,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.all(1),
                                                    alignment: Alignment.bottomLeft,
                                                    child: IconButton(
                                                      icon: Icon(FontAwesomeIcons.solidHeart,color: e.favorito == true? Colors.red:Colors.white60,),
                                                      onPressed: (){
                                                        setState(() {
                                                          e.favorito =true;
                                                        });
                                                        favoritos.add(e);
                                                      },
                                                    ))
                                                ],
                                              )),
                                        ),
                                      ))
                                  .toList(),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }

  List<AmiiboModel> contadorList(int fin) {
    List<AmiiboModel> lis = List<AmiiboModel>.empty();
    if (textBuscar.text.isNotEmpty) {
      lis = amibo
          .where((element) => element.name.contains(textBuscar.text))
          .toList();
    } else {
      lis = amibo.getRange(0, fin).toList();
    }
    return lis;
  }
}
