import 'dart:convert';
import 'dart:io';

import 'package:flutter_application_1/src/models/amiibo_model.dart';
import 'package:flutter_application_1/src/static/globals.dart';
import 'package:http/http.dart' as http;

Future<List<AmiiboModel>> obtenerAmiibo() async {
  final response = await http.get(
    "$direccion/amiibo",
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      "Accept": "aplication.json"
    },
  );
  var a = json.decode(response.body);
  print(a);
  return amiiboModelsFromJson(json.encode(a["amiibo"]));
}

Future<List<AmiiboModel>> inforMacionAmiibo(String name) async {
  final response = await http.get(
    "$direccion/amiibo/?na",
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      "Accept": "aplication.json"
    },
  );
  var a = json.decode(response.body);
  print(a);
  return amiiboModelsFromJson(json.encode(a["amiibo"]));
}
